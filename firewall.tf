resource "google_compute_firewall" "ssh" {
  name    = "allow-ssh"
  network = "${google_compute_network.prod.name}"

  direction     = "INGRESS"
  source_ranges = ["66.196.29.212/32"]
  target_tags   = ["api", "www"]

  allow {
    protocol = "tcp"
    ports    = ["22"]
  }
}
