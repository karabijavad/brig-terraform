resource "google_container_cluster" "prod" {
  provider                    = "google-beta"
  name                        = "prod"
  zone                        = "${var.zone}"
  min_master_version          = "1.11.3-gke.18"
  enable_binary_authorization = true
  enable_legacy_abac          = false
  network                     = "${google_compute_network.prod.self_link}"
  subnetwork                  = "${google_compute_subnetwork.prod.self_link}"

  master_authorized_networks_config = {
    cidr_blocks = [
      {
        display_name = "house"
        cidr_block   = "108.249.99.92/32"
      },
      {
        display_name = "tether"
        cidr_block   = "173.127.0.108/32"
      },
      {
        display_name = "kesten"
        cidr_block   = "66.196.29.212/32"
      },
      {
        display_name = "hookah"
        cidr_block   = "75.43.19.184/32"
      },
      {
        display_name = "tether"
        cidr_block   = "184.203.68.13/32"
      },
    ]
  }

  lifecycle {
    ignore_changes = ["node_pool", "network", "subnetwork"]
  }

  node_pool {
    name = "default-pool"
  }

  master_auth = {
    username = ""
    password = ""
  }

  network_policy {
    provider = "CALICO"
    enabled  = true
  }

  ip_allocation_policy {
    cluster_secondary_range_name  = "pods"
    services_secondary_range_name = "services"
  }
}
