resource "google_compute_network" "prod" {
  name                    = "prod"
  auto_create_subnetworks = false
}

resource "google_compute_subnetwork" "prod" {
  name          = "prod"
  region        = "${var.region}"
  ip_cidr_range = "10.128.0.0/20"
  network       = "${google_compute_network.prod.self_link}"

  secondary_ip_range {
    range_name    = "pods"
    ip_cidr_range = "10.96.0.0/11"
  }

  secondary_ip_range {
    range_name    = "services"
    ip_cidr_range = "10.94.0.0/18"
  }
}
