provider "google" {
  credentials = "${file("./credentials.json")}"
  project     = "${var.project}"
  region      = "${var.region}"
}

provider "google-beta" {
  credentials = "${file("./credentials.json")}"
  project     = "${var.project}"
  region      = "${var.region}"
}
