provider "helm" {
  install_tiller  = true
  service_account = "${google_service_account.tiller.unique_id}"

  kubernetes {
    host                   = "https://${google_container_cluster.prod.endpoint}"
    cluster_ca_certificate = "${base64decode(google_container_cluster.prod.master_auth.0.cluster_ca_certificate)}"

    token = "ya29.GlxlBlAKMqPswaXU2s_cKCaekwlYMY_AW-juC8Fv-CPsjdl8mJg6gqKwC2jZFRpaS_aql6Ff7783ggTZUTAZC77XP-THJaJPUt1ngieZyKBT3CZSdiVDEH6t1mlBtQ"
  }
}

resource "helm_release" "my_database" {
  name  = "my_datasase"
  chart = "stable/mariadb"

  set {
    name  = "mariadbUser"
    value = "foo"
  }

  set {
    name  = "mariadbPassword"
    value = "qux"
  }
}
