resource "google_container_node_pool" "www" {
  provider           = "google-beta"
  name               = "www"
  zone               = "${var.zone}"
  cluster            = "${google_container_cluster.prod.name}"
  initial_node_count = 1

  autoscaling {
    min_node_count = 3
    max_node_count = 7
  }

  node_config {
    tags = ["www"]

    labels = {
      tier = "www"
    }

    oauth_scopes = [
      "https://www.googleapis.com/auth/monitoring",
      "https://www.googleapis.com/auth/devstorage.read_only",
      "https://www.googleapis.com/auth/logging.write",
      "https://www.googleapis.com/auth/compute",
    ]

    preemptible     = true
    service_account = "${google_service_account.www.unique_id}"
  }
}

resource "google_container_node_pool" "api" {
  provider = "google-beta"
  name     = "api"
  zone     = "${var.zone}"
  cluster  = "${google_container_cluster.prod.name}"

  initial_node_count = 1

  autoscaling {
    min_node_count = 3
    max_node_count = 7
  }

  node_config {
    tags = ["api"]

    labels = {
      tier = "api"
    }

    oauth_scopes = [
      "https://www.googleapis.com/auth/monitoring",
      "https://www.googleapis.com/auth/devstorage.read_only",
      "https://www.googleapis.com/auth/logging.write",
      "https://www.googleapis.com/auth/compute",
    ]

    preemptible     = true
    service_account = "${google_service_account.api.unique_id}"
  }
}
