resource "google_service_account" "www" {
  account_id = "www-node"
}

# resource "google_project_iam_member" "www_binding_1" {
#   role   = "roles/logging.logWriter"
#   member = "serviceAccount:${google_service_account.www.email}"
# }

# resource "google_project_iam_member" "www_binding_2" {
#   role   = "roles/monitoring.metricWriter"
#   member = "serviceAccount:${google_service_account.www.email}"
# }

# resource "google_project_iam_member" "www_binding_3" {
#   role   = "roles/monitoring.viewer"
#   member = "serviceAccount:${google_service_account.www.email}"
# }

# resource "google_project_iam_member" "www_binding_4" {
#   role   = "roles/storage.objectViewer"
#   member = "serviceAccount:${google_service_account.www.email}"
# }

resource "google_service_account" "api" {
  account_id = "api-node"
}

# resource "google_project_iam_member" "www_binding_1" {
#   role   = "roles/logging.logWriter"
#   member = "serviceAccount:${google_service_account.www.email}"
# }

# resource "google_project_iam_member" "www_binding_2" {
#   role   = "roles/monitoring.metricWriter"
#   member = "serviceAccount:${google_service_account.www.email}"
# }

# resource "google_project_iam_member" "www_binding_3" {
#   role   = "roles/monitoring.viewer"
#   member = "serviceAccount:${google_service_account.www.email}"
# }

# resource "google_project_iam_member" "www_binding_4" {
#   role   = "roles/storage.objectViewer"
#   member = "serviceAccount:${google_service_account.www.email}"
# }

resource "google_service_account" "tiller" {
  account_id = "tiller"
}

resource "google_project_iam_member" "tiller_admin_binding" {
  role   = "roles/owner"
  member = "serviceAccount:${google_service_account.tiller.email}"
}
